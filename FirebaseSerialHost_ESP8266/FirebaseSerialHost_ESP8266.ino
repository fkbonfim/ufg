#include <Firebase.h>
#include <FirebaseArduino.h>
#include <FirebaseCloudMessaging.h>
#include <FirebaseError.h>
#include <FirebaseHttpClient.h>
#include <FirebaseObject.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#define FIREBASE_HOST "distanciacomurg.firebaseio.com"
#define FIREBASE_AUTH "WZJxw1HbsC8HrWdPtI59twYAJKM86P0a0q4uwq1i"
#define WIFI_SSID "FRANKLIN"
#define WIFI_PASSWORD "40878150"
#include <SoftwareSerial.h>         // including the library for the software serial
#define DEBUG true
SoftwareSerial esp8266(6,7);      /* This will make the pin 10 of arduino as RX pin and
pin 11 of arduino as the TX pin Which means that you have to connect the TX from the esp8266
to the pin 10 of arduino and the Rx from the esp to the pin 11 of the arduino*/
                                   
const int trigPin = 8;            // Making the arduino's pin 8 as the trig pin of ultrasonic sensor
const int echoPin = 9;            // Making the arduino's pin 9 as the echo pin of the ultrasonic sensor
// defining two variable for measuring the distance
long duration;
int distance;
void setup()
{
  Serial.begin(9600);         // Setting the baudrate at 9600
  esp8266.begin(9600);        // Set the baudrate according to you esp's baudrate. your esp's baudrate might be different from mine*/
  pinMode(trigPin, OUTPUT);   // Setting the trigPin as Output pin
  pinMode(echoPin, INPUT);    // Setting the echoPin as Input pin

 WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
 // Firebase.setInt("LEDStatus",0);


}

int n=0;
int ultra()
{
digitalWrite(trigPin, LOW);   // Making the trigpin as low
delayMicroseconds(2);         // delay of 2us
digitalWrite(trigPin, HIGH); // making the trigpin high for 10us to send the signal 
delayMicroseconds(10);
digitalWrite(trigPin, LOW);   
duration = pulseIn(echoPin, HIGH);  // reading the echopin which will tell us that how much time the signal takes to come back 

distance= duration*0.034/2;         // Calculating the distance and storing in the distance variable
Serial.print("Distance: "); 
Serial.println(distance); 


}
void loop() {
  ultra();
  // set value
  Firebase.setFloat("distance", 10.0);
  // handle error
  if (Firebase.failed()) {
      Serial.print("setting /number failed:");
      Serial.println(Firebase.error());  
      return;
  }
  delay(1000);
  
  // update value
  Firebase.setFloat("distance", 8.0);
  // handle error
  if (Firebase.failed()) {
      Serial.print("setting /number failed:");
      Serial.println(Firebase.error());  
      return;
  }
  delay(1000);

  // get value 
  Serial.print("distance: ");
  Serial.println(Firebase.getFloat("distance"));
  delay(1000);

  // remove value
  Firebase.remove("distance");
  delay(1000);

  // set string value
  Firebase.setString("message", "bin is full");
  // handle error
  if (Firebase.failed()) {
      Serial.print("setting /message failed:");
      Serial.println(Firebase.error());  
      return;
  }
  delay(1000);
  
  // set bool value
  Firebase.setBool("truth", false);
  // handle error
  if (Firebase.failed()) {
      Serial.print("setting /truth failed:");
      Serial.println(Firebase.error());  
      return;
  }
  delay(1000);

  // append a new value to /logs
  String name = Firebase.pushInt("logs", n++);
  // handle error
  if (Firebase.failed()) {
      Serial.print("pushing /logs failed:");
      Serial.println(Firebase.error());  
      return;
  }
  Serial.print("pushed: /logs/");
  Serial.println(name);
  delay(1000);
}
