// Thingspeak.ino: comunicacao entre Arduino e a Nuvem!
//
// Baseado no programa esp8266_test de Mahesh Venkitachalam

#include "DHT.h"
#include <stdlib.h>
#include <SoftwareSerial.h>

int pinoLED = 13;
int pinoLDR = A0;
int pinoDHT = 8;
int tipoDHT =  DHT11;

DHT dht(pinoDHT, tipoDHT); // Inicializa o sensor
SoftwareSerial EspSerial(6, 7); // Inicializa a porta de comunicacao com modulo WiFi

// substitua pela chave de leitura do seu canal no Thingspeak
String chaveDeEscrita = "Q7UHBWDO6T1XQZGE";

void setup() {                
  pinMode(pinoLED, OUTPUT);
  Serial.begin(9600); // Comunicacao com Monitor Serial
  EspSerial.begin(9600); // Comunicacao com Modulo WiFi
  dht.begin();
  EnviarComando("AT+RST", "Ready"); //Reset do Modulo WiFi
}

void loop() {
  float luminosidade = 1024.0f - analogRead(pinoLDR); // valores baixos: luminosidade baixa
  float umidade = dht.readHumidity();
  float temperatura = dht.readTemperature();

  // converte leiturasnpara String
  String strLuminosidade = floatToString(luminosidade);
  String strUmidade = floatToString(umidade);
  String strTemperatura = floatToString(temperatura);
 
  // Conexao com TCP com Thingspeak
  String cmd = "AT+CIPSTART=\"TCP\",\"";
  cmd += "184.106.153.149"; // Endereco IP de api.thingspeak.com
  cmd += "\",80";
  EspSerial.println(cmd);

  if(EspSerial.find("Error")){
    Serial.println("AT+CIPSTART error");
    return;
  }

  // preparacao da string GET
  String getStr = "GET /update?api_key=";
  getStr += chaveDeEscrita;
  getStr +="&field1=";
  getStr += String(strLuminosidade);
  getStr +="&field2=";
  getStr += String(strUmidade);
  getStr +="&field3=";
  getStr += String(strTemperatura);
  getStr += "\r\n\r\n";

  // envia o tamanho dos dados
  cmd = "AT+CIPSEND=";
  cmd += String(getStr.length());
  EspSerial.println(cmd);

  // envia comando de GET
  if(EspSerial.find(">")){
    EspSerial.print(getStr);
  }
  else{
    EspSerial.println("AT+CIPCLOSE");
    // alerta usuario
    Serial.println("AT+CIPCLOSE");
  }

  Serial.println("Comando enviado ao Thingspeak:");
  Serial.println(getStr);

  // pisca o LED a cada envio
  digitalWrite(pinoLED, HIGH);   
  delay(200);               
  digitalWrite(pinoLED, LOW);

  // o thingspeak precisa de pelo menos 16 segundos entre atualizacoes
  delay(16000);  
}

String floatToString(float f) {
  char buf[16];
  return dtostrf(f, 4, 1, buf);
}

boolean EnviarComando(String cmd, String ack){
  EspSerial.println(cmd); // Envia comando "AT+" para o modulo
  if (!echoFind(ack)) // tempo de espera para string de ack
    return true; // ack vazio ou ack encontrado
}

boolean echoFind(String keyword){
 byte current_char = 0;
 byte keyword_length = keyword.length();
 long deadline = millis() + 5000; // Tempo de espera 5000ms
 while(millis() < deadline){
  if (EspSerial.available()){
    char ch = EspSerial.read();
    Serial.write(ch);
    if (ch == keyword[current_char])
      if (++current_char == keyword_length){
       Serial.println();
       return true;
    }
   }
  }
 return false; // Tempo de espera esgotado
}
