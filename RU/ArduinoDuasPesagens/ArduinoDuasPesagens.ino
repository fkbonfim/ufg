#include "HX711.h"
HX711 scale1(A1, A0); // DOUT, SCK
HX711 scale2(A3, A2); // DOUT, SCK

//
void setup()
{
  Serial.begin(9600);
  scale1.set_scale(2280.f); //// this value is obtained by calibrating the scale with known weights
  scale1.tare();

  scale2.set_scale(2280.f); //// this value is obtained by calibrating the scale with known weights
  scale2.tare();
}

void loop()
{
  float soma = 0;
  Serial.print("one reading:\t");
  Serial.println(scale1.get_units(), 2);
  scale1.power_down(); // put the ADC in sleep mode

  Serial.print("two reading:\t");
  Serial.println(scale2.get_units(), 2);
  scale2.power_down(); // put the ADC in sleep mode



  delay(1000);
  scale1.power_up();
  scale2.power_up();
  soma = (scale1.get_units()) + (scale2.get_units());
  Serial.print("Soma:\t");
  Serial.println(soma);
  Serial.println("");

}
