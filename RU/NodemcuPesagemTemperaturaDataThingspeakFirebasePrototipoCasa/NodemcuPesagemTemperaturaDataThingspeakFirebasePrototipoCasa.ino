/*Prototipo casa ESP8266
   1) Pesagem (teste)PORTA DIGITAL
   2) Temperatura Umidade
   3) Data hora
   4) 1 ultrasonico
   5) gas com bafometro
   Servidores
   1 - ThingSpeak
   2 - Firebase
*/

#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>
#include <Ticker.h>
#include "DHT.h"
#include "HX711.h"
#include <TimeLib.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#define DHTPIN D3 // pino que estamos conectado
#define DHTTYPE DHT11 // DHT 11

#define FIREBASE_HOST "prototipo-b598c.firebaseio.com"
#define FIREBASE_AUTH "J5fPzatKWDcOv6sU88uurjLxfNFpD46BDpcqjRZH"

//A cada um hora
//#define PUBLISH_INTERVAL 1000*60*60
#define PUBLISH_INTERVAL 1000*60

// NTP Servers:
static const char ntpServerName[] = "a.st1.ntp.br";
const int timeZone = -3;     // BRAZIL
WiFiUDP Udp;
unsigned int localPort = 8888;  // local port to listen for UDP packets
time_t getNtpTime();
void digitalClockDisplay();
void printDigits(int digits);
void sendNTPpacket(IPAddress &address);


// configurando pesagem portas digitais
const int SCALE_DOUT_PIN = D2;
const int SCALE_SCK_PIN = D3;

HX711 scale(SCALE_DOUT_PIN, SCALE_SCK_PIN);

int trigPin1 = 14;//d5 direita
int echoPin1 = 12;//d6 direita

//int trigPin2 = 5;//d1 esquerda
//int echoPin2 = 4;//d2 esquerda

int prfdde1 = 15; // profundidade da caixa (aqui vc coloca a pronfudidade em cm
//int prfdde2 = 96; // profundidade da caixa (aqui vc coloca a pronfudidade em cm

//teor de gas/alcool
int gas = A0;
int nivel_sensor = 250;


//Definir o SSID da rede WiFi
const char* ssid = "FRANKLIN";
//Definir a senha da rede WiFi
const char* password = "40878150";

////////
DHT dht(DHTPIN, DHTTYPE);

Ticker ticker;
bool publishNewState = true;

void publish() {
  publishNewState = true;
}

void setupFirebase() {
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  //Firebase.setBool("lamp", false);
  //Firebase.setBool("presence", false);
}

//Colocar a API Key para escrita neste campo
//Ela é fornecida no canal que foi criado na aba API Keys
String apiKey = "EESZ7NJWD7ZLV7JM";
const char* server = "api.thingspeak.com";

WiFiClient client;

void setup() {
  // Configuração da UART
  Serial.begin(9600);

  //Inicia o WiFi
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  //Logs na porta serial
  Serial.println("");
  Serial.print("Conectado na rede ");
  Serial.println(ssid);
  Serial.print("IP: ");
  Serial.println(WiFi.localIP());

  //Configurando DATA e Hora///////////////////////////////////////////////////////////////////
  Serial.print("IP number assigned by DHCP is ");
  Serial.println(WiFi.localIP());
  Serial.println("Starting UDP");
  Udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(Udp.localPort());
  Serial.println("waiting for sync");
  setSyncProvider(getNtpTime);
  setSyncInterval(300);

  //pesagem///////////////////////////////////////////////////////////////////////////////////
  Serial.println("Leitura antes de configurar a escala");
  Serial.print("Leitura: \t\t");
  Serial.println(scale.read());     // Imprimindo a escala atual
  scale.set_scale(-82780.f);// <- Configurando a calibração!!!
  scale.tare();

  //gas//////////////////////////////////////////////////////////////////////////////////////
  pinMode(gas, INPUT);

  //ultrassonico///////////////////////////////////////////////////////////////////////////////
  pinMode(trigPin1, OUTPUT);
  pinMode(echoPin1, INPUT);
  // pinMode(trigPin2, OUTPUT);
  // pinMode(echoPin2, INPUT);

  dht.begin();

  setupFirebase();

  // Registra o ticker para publicar de tempos em tempos
  ticker.attach_ms(PUBLISH_INTERVAL, publish);

}

//data e hora/////////////////////////////////////////////////////////////////////////////
time_t prevDisplay = 0; // when the digital clock was displayed

void loop() {
  //Data e HOra////////////////////////////////////////////////////////////////////////////
  if (timeStatus() != timeNotSet) {
    if (now() != prevDisplay) { //update the display only if time has changed
      prevDisplay = now();
      digitalClockDisplay();

    }
  }
  //Configurando SCALA Pesagem///////////////////////////////////////////////////////////
  float weight = scale.get_units(1) * 10;
  Serial.print(String(weight, 2)); // Sting(valor,casas_decimais)
  Serial.println(" Kg");
  //Serial.println("");
  scale.power_down();             // put the ADC in sleep mode
  delay(300);
  scale.power_up();

  //ULTRASONICO///////////////////////////////////////////////////////////////////////////////////////////////////
  long duration1, distance1;
  digitalWrite(trigPin1, LOW);  // Added this line
  delayMicroseconds(2); // Added this line
  digitalWrite(trigPin1, HIGH);
  delayMicroseconds(10); // Added this line
  digitalWrite(trigPin1, LOW);
  duration1 = pulseIn(echoPin1, HIGH);
  distance1 = (duration1 / 2) / 29.1;
  long nivel1 = 100 - (distance1 * 100 / prfdde1); // variave nivel atribui o a valor da distancia e calcula
  if (distance1 >= 500 || distance1 <= 0) {
    Serial.println("Out of range");
  }
  else {
    Serial.print ( "Distancia: ");
    Serial.print ( distance1);
    Serial.println("cm");
    Serial.print ("Nivel:");
    Serial.print ( nivel1);
    Serial.println("%");
    Serial.println("");

  }
  delay(300);

  /*DIREITA
    long duration2, distance2;
    digitalWrite(trigPin2, LOW);  // Added this line
    delayMicroseconds(2); // Added this line
    digitalWrite(trigPin2, HIGH);
    delayMicroseconds(10); // Added this line
    digitalWrite(trigPin2, LOW);
    duration2 = pulseIn(echoPin2, HIGH);
    distance2 = (duration2 / 2) / 29.1;
    long nivel2 = 100 - (distance2 * 100 / prfdde2); // variave nivel atribui o a valor da distancia e calcula
    if (distance2 >= 500 || distance2 <= 0) {
    Serial.println("Out of range");
    }
    else {
    Serial.print("Sensor2: ");
    Serial.print(distance2);
    Serial.println("cm");
    Serial.print ("Lado esquerdo: ");
    Serial.print ( nivel2);
    Serial.println("%");
    }
  */
  //Serial.println();
  //Serial.println();

  // A leitura da temperatura e umidade pode levar 250ms!
  // O atraso do sensor pode chegar a 2 segundos.
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  // testa se retorno é valido, caso contrário algo está errado.
  if (isnan(t) || isnan(h))
  {
    Serial.println("Failed to read from DHT");
  }
  else
  {
    Serial.println("_____________________________________");
    Serial.print("Umidade: ");
    Serial.print(h);
    Serial.println(" %t");
    Serial.print("Temperatura: ");
    Serial.print(t);
    Serial.println(" *C");
    delay(300);
  }


  //Sensor gas//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  int valor_analogico = analogRead(gas);
  Serial.print("Gas : ");
  Serial.println(valor_analogico);
  //Serial.println();

  //THINGSPEAK///////////////////////////////////////////////////////////////////////////////////////////////////////
  //Inicia um client TCP para o envio dos dados
  if (client.connect(server, 80)) {
    String postStr = apiKey;
    postStr += "&amp;field1=";
    postStr += String(valor_analogico);
    postStr += "&amp;field2=";
    postStr += String(nivel1);
    postStr += "&amp;field3=";
    postStr += String(h);
    postStr += "&amp;field4=";
    postStr += String(t);
    postStr += "&amp;field5=";
    postStr += String(weight);
    postStr += "&amp;field6=";
    postStr += String(now());
    postStr += "\r\n\r\n";

    client.print("POST /update HTTP/1.1\n");
    client.print("Host: api.thingspeak.com\n");
    client.print("Connection: close\n");
    client.print("X-THINGSPEAKAPIKEY: " + apiKey + "\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("Content-Length: ");
    client.print(postStr.length());
    client.print("\n\n");
    client.print(postStr);
  }

  //Acessando Firebase/////////////////////////////////////////////////////////////////////////////////////////////
  client.stop();
  if (publishNewState) {
    Serial.println("Publish new State");
    // Obtem os dados do sensor DHT
    float humidity = dht.readHumidity();
    float temperature = dht.readTemperature();
    float peso = weight;
    //int nivel1 = nivel1;
    int gas = valor_analogico;
    //int nivel2 = nivel2;
    int datahora = now();


    //if (!isnan(nivel1) && !isnan(nivel2) && !isnan(humidity) && !isnan (temperature)) {
    if (isnan(humidity) && !isnan (temperature)) {
      //Firebase.pushFloat("Gas: ", gas);
      Firebase.pushFloat("Volume: ", nivel1);
      Firebase.pushFloat("Umidade: ", humidity);
      Firebase.pushFloat("Temperatura: ", temperature);
      //Firebase.pushFloat("Peso: ", weight);
      Firebase.pushInt("Data: ", datahora);
      publishNewState = false;

    } else {
      Serial.println("Error Publishing");
    }
  }

  //delay(1800000);//1hora //meia hora
  delay(1000);//1hora //meia hora
}

//Configurando Data e Hora////////////////////////////////////////////////////////////////////////////////
void digitalClockDisplay()
{
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.print(" ");
  Serial.print(dayStr(weekday())); //dia da semana
  Serial.print(" ");
  Serial.print(day());
  Serial.print(".");
  Serial.print(month());
  Serial.print(".");
  Serial.print(year());
  Serial.println();
  Serial.print("data: ");
  Serial.println(now());
  //Serial.println("");
  delay (300);
}

void printDigits(int digits)
{
  // utility for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

/*-------- NTP code ----------*/

const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets

time_t getNtpTime()
{
  IPAddress ntpServerIP; // NTP server's ip address

  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  Serial.println("Transmit NTP Request");
  // get a random server from the pool
  WiFi.hostByName(ntpServerName, ntpServerIP);
  Serial.print(ntpServerName);
  Serial.print(": ");
  Serial.println(ntpServerIP);
  sendNTPpacket(ntpServerIP);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Serial.println("Receive NTP Response");
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
    }
  }
  Serial.println("No NTP Response :-(");
  return 0; // return 0 if unable to get the time
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress &address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}
