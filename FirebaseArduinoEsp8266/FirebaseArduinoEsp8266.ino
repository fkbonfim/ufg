#include <stdlib.h>

#include "SoftwareSerial.h"

String chaveDeEscrita = "INSIRA_AQUI_SUA_CHAVE_DE_ESCRITA_DO_THINGSPEAK";

#define FIREBASE_HOST "distanciacomurg.firebaseio.com"
#define FIREBASE_AUTH "WZJxw1HbsC8HrWdPtI59twYAJKM86P0a0q4uwq1i"

#define TIMEOUT 5000 // mS
#define LED 13

SoftwareSerial mySerial(6, 7); // RX, TX
const int button = 8;
const int LDR = A0;
int LDR_value = 0;

void setup()
{
  pinMode(LED, OUTPUT);
  Serial.begin(9600); // Comunicacao com Monitor Serial
  EspSerial.begin(9600); // Comunicacao com Modulo WiFi
  dht.begin();
  EnviarComando("AT+RST", "Ready"); //Reset do Modulo WiFi

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);

}

void loop() {
  // set value
  Firebase.setFloat("number", 42.0);
  // handle error
  if (Firebase.failed()) {
    Serial.print("setting /number failed:");
    Serial.println(Firebase.error());
    return;
  }
  delay(1000);

  // update value
  Firebase.setFloat("number", 43.0);
  // handle error
  if (Firebase.failed()) {
    Serial.print("setting /number failed:");
    Serial.println(Firebase.error());
    return;
  }
  delay(1000);

  // get value
  Serial.print("number: ");
  Serial.println(Firebase.getFloat("number"));
  delay(1000);

  // remove value
  Firebase.remove("number");
  delay(1000);

  // set string value
  Firebase.setString("message", "hello world");
  // handle error
  if (Firebase.failed()) {
    Serial.print("setting /message failed:");
    Serial.println(Firebase.error());
    return;
  }
  delay(1000);

  // set bool value
  Firebase.setBool("truth", false);
  // handle error
  if (Firebase.failed()) {
    Serial.print("setting /truth failed:");
    Serial.println(Firebase.error());
    return;
  }
  delay(1000);

  // append a new value to /logs
  String name = Firebase.pushInt("logs", n++);
  // handle error
  if (Firebase.failed()) {
    Serial.print("pushing /logs failed:");
    Serial.println(Firebase.error());
    return;
  }
  Serial.print("pushed: /logs/");
  Serial.println(name);
  delay(1000);
}

boolean SendCommand(String cmd, String ack) {
  mySerial.println(cmd); // Send "AT+" command to module
  if (!echoFind(ack)) // timed out waiting for ack string
    return true; // ack blank or ack found
}

boolean echoFind(String keyword) {
  byte current_char = 0;
  byte keyword_length = keyword.length();
  long deadline = millis() + TIMEOUT;
  while (millis() < deadline) {
    if (mySerial.available()) {
      char ch = mySerial.read();
      Serial.write(ch);
      if (ch == keyword[current_char])
        if (++current_char == keyword_length) {
          Serial.println();
          return true;
        }
    }
  }
  return false; // Timed out
}
